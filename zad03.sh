#!/bin/bash

docker run -d --name baza-mongo -p 27017:27017 mongo

sleep 10

docker run -d --name zadanie3 -p 8080:8080 -v ./server.js:/app/server.js --link baza-mongo:mongodb node:16 /bin/bash -c "cd app && npm init -y && npm install express mongoose && node server.js"

