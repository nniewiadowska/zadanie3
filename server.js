const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 8080;

mongoose.connect("mongodb://mongodb:27017/zad03", 
	{ useNewUrlParser: true, useUnifiedTopology: true }
).then(() => {
	console.log("MongoDB connection established!");
}).catch(err => console.log(err));

const Sample = mongoose.model("Sample", new mongoose.Schema({
  name: String,
  value: Number
}));

app.get("/", async (req, res) => {
	try {
		const randomData = [
			{ name: "przykład 1", value: 73 },
			{ name: "przykład 2", value: 12 }
		];

    		await Sample.insertMany(randomData);
		const result = await Sample.find();

		res.json(result);
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal Server Error" });
	}
});

app.listen(port, () => {
	console.log(`Server running at http://0.0.0.0:${port}/`);
});
